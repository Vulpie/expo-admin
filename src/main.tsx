import React from 'react'
import ReactDOM from 'react-dom/client'
import Layout from './components/ui/Layout.tsx';
import Home from './components/pages/Home.tsx';
import Stall from './components/pages/Stall.tsx';
import User from './components/pages/User.tsx';
import './index.css'
import {
    createBrowserRouter,
    RouterProvider,
} from "react-router-dom";

const router = createBrowserRouter([
    {
        path: "/",
        element: <Layout />,
        children: [
            {
                path: "/",
                element: <Home />,
            },
            {
                path: "/stall",
                element: <Stall />,
            },
            {
                path: "/user",
                element: <User />,
            },
        ],
    },
]);

ReactDOM.createRoot(document.getElementById('root')!).render(
    <React.StrictMode>
        <RouterProvider router={router} />
    </React.StrictMode>,
)

import React from 'react'
import Button from '@mui/material/Button';

type Props = {}

const Home = (props: Props) => {
    return (
        <div>
            Home
            <Button variant="contained">Hello world</Button>
        </div>
    )
}

export default Home
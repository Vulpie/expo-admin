import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';

type Props = {}

const Stall = (props: Props) => {
    return (
        <Box
            component="form"
            sx={{
                '& > :not(style)': { m: 1, width: '25ch' },
            }}
            noValidate
            autoComplete="off"
        >
            <TextField label="Outlined" variant="outlined" />
        </Box>
    )
}

export default Stall